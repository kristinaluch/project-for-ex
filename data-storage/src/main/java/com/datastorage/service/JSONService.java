package main.java.com.datastorage.service;

import com.google.gson.Gson;
import main.java.com.datastorage.models.Person;
import netscape.javascript.JSObject;


import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class JSONService {

    private Scanner scanner = new Scanner(System.in);
    private MyClassServ myClassServ = new MyClassServ();
    private static final String PATH = "data-storage/src/jsonFile.json";
    private static final String PATH2 = "data-storage/src.jsonFile2.json";
    private File jsonFile = new File(PATH);
    private Gson gson = new Gson();

    public void create(){
        MyClassServ myClassServ = new MyClassServ();
        Person person = myClassServ.createPerson();
        String json = gson.toJson(person);
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(PATH, true))) {
            bufferedWriter.write(json+"\n");
            System.out.println("Person save");
            bufferedWriter.flush();
        } catch (IOException e) {
            System.out.println("my error js");
            e.printStackTrace();
        }
    }

    public void update(){
        int id = myClassServ.printId(); //��� � ���� ��. ����� ������ ��������.
        File jsonFile2 = new File(PATH2); // ������ ����� ����, � ������� ���� ��������������
        try(BufferedReader br = new BufferedReader(new FileReader(PATH)))
        {//������ ���������
            String s; //���� ������
            while((s=br.readLine())!=null){ //���� ��� �� ������, ���������
                if(!s.contains("\"id\":"+id)){ //���� ��� ������ �� ����� ���������� ��
                    rewrite(s, PATH2);//��������� ���������� ����� �����(jsonFile) �� ������ ����(jsonFile2)
                }
                else {
                    Person updatePerson = gson.fromJson(s, Person.class);//����� ��� ���������: ������ ������������
                    //� ������ Person � ������� GSON(��� ������)
                    updatePerson.setFname(scanner.next()); //�������� ��� ������, ����� ��
                    updatePerson.setLname(scanner.next());
                    updatePerson.setAge(scanner.nextInt());
                    updatePerson.setCity(scanner.next());
                    String updateJSON = gson.toJson(updatePerson);//��������� ������ � �������-������
                    rewrite(updateJSON, PATH2);//�������� ������� ������ � ����
                }
            }
        }
        catch(IOException e){
            System.out.println("my error js");
            e.printStackTrace();
        }
        jsonFile.delete();
        boolean success = jsonFile2.renameTo(jsonFile);

        if (success){
            System.out.println("Person update");
        }
        else
        {
            System.out.println("Error file");
        }


    }
    private void rewrite(String json, String pathFileToWrite){
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(pathFileToWrite, true))) {//������ ����� ������
            //������ ����������� true - ����� ��� ������������ � �����, � �� ������� ��� ���������
            bufferedWriter.write(json+"\n");//�������� ������� ������ � ������� ������
            bufferedWriter.flush();//��������
        } catch (IOException e) {//������������ �����
            System.out.println("my error js");
            e.printStackTrace();
        }
    }
    public void delete(){
        int id = myClassServ.printId();
        File jsonFile2 = new File(PATH2);
        try(BufferedReader br = new BufferedReader(new FileReader(PATH)))
        {//������ ���������
            String s;
            while((s=br.readLine())!=null){
                if(!s.contains("\"id\":"+id)){
                    rewrite(s, PATH2);
                }
            }
        }
        catch(IOException e){
            System.out.println("my error js");
            e.printStackTrace();
        }
        jsonFile.delete();
        boolean success = jsonFile2.renameTo(jsonFile);

        if (success){
            System.out.println("Person delete");
        }
        else
        {
            System.out.println("Error file");
        }

    }
    public void readID(){
        int id = myClassServ.printId();
        try(BufferedReader br = new BufferedReader(new FileReader(PATH)))
        {//������ ���������
            String s;
            while((s=br.readLine())!=null){

                if(s.contains("\"id\":"+id)){
                    Person read = gson.fromJson(s, Person.class);
                    System.out.println(read.toString());
                    return;
                }
            }
            System.out.println("ID not found");
            return;
        }
        catch(IOException e){
            System.out.println("my error js");
            e.printStackTrace();
        }
    }
    public void readAll(){
        try(BufferedReader br = new BufferedReader(new FileReader(PATH)))
        {//������ ���������
            String s;
            while((s=br.readLine())!=null){
                Person read = gson.fromJson(s, Person.class);
                System.out.println(read.toString());
            }
        }
        catch(IOException e){
            System.out.println("my error js");
            e.printStackTrace();
        }
    }


}
