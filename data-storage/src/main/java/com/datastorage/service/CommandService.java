package main.java.com.datastorage.service;

import java.util.Scanner;

public class CommandService {
    private static final String MSG_WELCOME = "Welcome to the Data Storage App!";
    private static final String MSG_AVAILABLE_COMMANDS = "Select: choose file, help or exit.";
    private static final String MSG_INVALID_COMMAND = "Invalid command!";
    private static final String MSG_ENTER_EXTENSION = "Enter file extension";
    private static final String MSG_SELECTED_EXTENSION = "You working with ";
    private static final String MSG_AVAILABLE_CRUD_COMMANDS = "Select: create, read, update, delete, switch, help or exit.";
    //private static final String MSG_SWITCH = "Select the extension you want to switch to: .xml, .binary, .csv, .yaml, .json.";
    private static final String MSG_EXTENSIONS = "Select extension: .xml, .binary, .csv, .yaml, .json.";
    private static final String MSG_CURRENT_EXTENSION = "You are currently working with: ";
    private static final String RESPONSE_HELP = "help";
    private static final String RESPONSE_EXIT = "exit";
    private static final String RESPONSE_CHOOSE_FILE = "choose file";
    private static final String RESPONSE_CREATE = "create";
    private static final String RESPONSE_READ = "read";
    private static final String RESPONSE_UPDATE = "update";
    private static final String RESPONSE_DELETE = "delete";
    private static final String RESPONSE_SWITCH = "switch";
    private static final String RESPONSE_NAME = "name";
    private static final String RESPONSE_XML = ".xml";
    private static final String RESPONSE_YAML = ".yaml";
    private static final String RESPONSE_BINARY = ".binary";
    private static final String RESPONSE_CSV = ".csv";
    private static final String RESPONSE_JSON = ".json";


    private Scanner scanner;
    private static String fileExtension;
    private static String userResponse;
    private static boolean flag = true;


    public CommandService(Scanner scanner){
        this.scanner = scanner;
    }

    public String scanResponse(){
        return scanner.nextLine();
    }

    public void startApp(){
        System.out.println(MSG_WELCOME);
        while(true) {
            System.out.println(MSG_AVAILABLE_COMMANDS);
            chooseStartCommand(scanResponse());
            //startCRUDMenu(scanResponse(), fileExtension);
        }
    }

    private void chooseStartCommand(String userResponse){
            if (userResponse.equalsIgnoreCase(RESPONSE_HELP))
                help();
            else if (userResponse.equalsIgnoreCase(RESPONSE_EXIT))
                exit();
            else if (userResponse.equalsIgnoreCase(RESPONSE_CHOOSE_FILE)) {
                chooseFile();
            } else {
                System.out.println(MSG_INVALID_COMMAND);
            }
    }

    private void chooseFile(){
        System.out.println(MSG_ENTER_EXTENSION);
        chooseExtension();
        System.out.println(MSG_SELECTED_EXTENSION + fileExtension);
        while(true) {
            startCRUDMenu(fileExtension);
        }
    }

    private void exit(){
        System.exit(0);
    }

    private void help(){
        System.out.println(RESPONSE_HELP);
    }

    private void startCRUDMenu(String fileExtension){
        System.out.println(MSG_AVAILABLE_CRUD_COMMANDS);
        userResponse = scanner.nextLine();
        switch(userResponse){
            case RESPONSE_CREATE:
                create(fileExtension);
                break;
            case RESPONSE_READ:
                read(fileExtension);
                break;
            case RESPONSE_UPDATE:
                update(fileExtension);
                break;
            case RESPONSE_DELETE:
                delete(fileExtension);
                break;
            case RESPONSE_HELP:
                help();
                break;
            case RESPONSE_NAME:
                System.out.println(MSG_CURRENT_EXTENSION+fileExtension);
                break;
            case RESPONSE_SWITCH:
                chooseExtension();
                break;
            case RESPONSE_EXIT:
                exit();
            default:
                System.out.println(MSG_INVALID_COMMAND);
        }
    }

    private void chooseExtension(){
        while(true) {
            System.out.println(MSG_EXTENSIONS);

            fileExtension = scanner.nextLine();

            switch (fileExtension) {
                case RESPONSE_XML:
                    return;
                case RESPONSE_YAML:
                    return;
                case RESPONSE_BINARY:
                    return;
                case RESPONSE_CSV:
                    return;
                case RESPONSE_JSON:
                    return;
                default:
                    System.out.println(MSG_INVALID_COMMAND);
                    break;
            }
        }
    }

    private void create(String fileExtension){
        System.out.println("CREATE");
        BinaryService binaryService = new BinaryService();
        binaryService.createNotation();
    }

    private void read(String fileExtension){
        System.out.println("READ");
    }

    private void update(String fileExtension){
        System.out.println("UPDATE");
    }

    private void delete(String fileExtension){
        System.out.println("DELETE");
    }
}
