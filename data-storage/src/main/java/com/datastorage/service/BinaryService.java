package main.java.com.datastorage.service;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import main.java.com.datastorage.service.CommandService;
import main.java.com.datastorage.models.Person;

public class BinaryService {

    private static final String MSG_ENTER_LNAME = "Enter first name: ";

    Scanner scanner = new Scanner(System.in);

    public BinaryService(){ }

    public void createNotation(){
        System.out.println(MSG_ENTER_LNAME);
        Person person = new Person();
        person.setFname(scanner.nextLine());
        person.setLname(scanner.nextLine());
        person.setAge(scanner.nextByte());
        person.setCity(scanner.nextLine());
        System.out.println(person.getFname());
        System.out.println(person.getLname());
        System.out.println(person.getAge());
        System.out.println(person.getCity());
    }

}
