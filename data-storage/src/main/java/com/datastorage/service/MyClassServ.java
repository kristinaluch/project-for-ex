package main.java.com.datastorage.service;

import main.java.com.datastorage.models.Person;

import java.util.Scanner;

public class MyClassServ {
    private Scanner scanner = new Scanner(System.in);

    public Person createPerson(){
        System.out.println("print id");
        Person person = new Person();
        person.setId(scanner.nextInt());
        person.setFname(scanner.next());
        person.setLname(scanner.next());
        person.setAge(scanner.nextInt());
        person.setCity(scanner.next());
        System.out.println(person.getId());
        System.out.println(person.getFname());
        System.out.println(person.getLname());
        System.out.println(person.getAge());
        System.out.println(person.getCity());

        return person;
    }

    public int printId(){
        System.out.println("print id");
        int id = scanner.nextInt();
        return id;
    }


}
