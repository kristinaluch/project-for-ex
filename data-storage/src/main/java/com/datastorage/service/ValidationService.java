package main.java.com.datastorage.service;

import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationService {

    private static final String MSG_INVALID_STRING = "Invalid string! The string must contain only letters!";
    private static final String MSG_INVALID_NUMBER = "Invalid number! The string must contain only number!";

    Scanner scanner = new Scanner(System.in);

    public String validateString(){
        String str;

        while(true){
            str = scanner.nextLine();
            Pattern pattern = Pattern.compile("[0-9`~!@#$%^&*()_+=?><,.;:{}\"]");
            Matcher matcher = pattern.matcher(str);
            if(matcher.find()){
                System.out.println(MSG_INVALID_STRING);
                continue;
            }
            break;
        }
        return str.trim();
    }

    public int validateNumber(){
        String number;

        while(true){
            number = scanner.nextLine();
            Pattern pattern = Pattern.compile("\\D");
            Matcher matcher = pattern.matcher(number);
            if(matcher.find()){
                System.out.println(MSG_INVALID_NUMBER);
                continue;
            }
            break;
        }
        return Integer.parseInt(number);
    }

}
