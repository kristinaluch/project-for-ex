package main.java.com.datastorage.models;

import main.java.com.datastorage.service.CommandService;

import java.util.Scanner;

public class Person {
    private int id;
    private String fname;
    private String lname;
    private int age;
    private String city;

    /*public Person( String fname, String lname, byte age, String city) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.age = age;
        this.city = city;
    }*/

    //��� ���������


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    //�����

    public Person() { }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", fname='" + fname + '\'' +
                ", lname='" + lname + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                '}';
    }
}
